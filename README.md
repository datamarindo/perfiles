## crea una serie de plots de barras o líneas con los perfiles orográficos del país que se desee

solo hay que cambiar los parámetros con los códigos ISO (MX para México, o RO para Romania, e.g.) del país y el nombre de este (para el título)

hay dos tipos de plot: 

## como barras: 


<img src="Rplot01.png"  width="300" height="300">


## como líneas:
<img src="perfilineas.png"  width="300" height="300">


Datamarindo: https://gitlab.com/datamarindo

