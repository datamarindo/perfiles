library(raster)

# para ver los códigos de país:
#     getData("ISO3")
alt = getData(name = "alt", country = "MEX") # p descargar las elevaciones del país
paso = 16                                    # cuántas líneas conformarán el plot
pais = "M\\'exico"                # para acentos ver:  demo(Hershey)
alt_max = max(alt[], na.rm = T)
# como barplot
aumento = nrow(alt)/paso
par(mfrow = c(paso + 1,1), bg = "#ECEBE6", mar = c(1,0,.5,0), fg = "#757472",
    family = "HersheySans")
plot.new()
for(i in 1:paso) { 
  barplot(alt[aumento*i,], ylim = c(0, alt_max))
}
mtext(sprintf("Perfiles de %s; gitlab.com/datamarindo", pais), 
      cex = 1.1, side = 3, line = -2, outer = TRUE, adj = 1)


# gráfica como líneas
aumento = nrow(alt)/paso
par(mfrow = c(paso + 1,1), bg = "#ECEBE6", mar = c(1,0,1,0), fg = "#757472",
    family = "HersheySans")
plot.new()
for(i in 1:paso) { 
  plot(xFromCol(alt),alt[aumento*i,], 
       type = "l", axes = F, xlab = "", ylab = "", bty = "n", ylim = c(0, alt_max))
}
mtext(sprintf("Perfiles de %s; gitlab.com/datamarindo", pais), 
      cex = 1.1, side = 3, line = -2, outer = TRUE, adj = 1)
